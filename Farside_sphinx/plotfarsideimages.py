


#!/usr/bin/env python

"""
This is the documentation file for the plot-farside-image.py file.  
Inline documentations are missing. 

"""

from __future__ import print_function, division
import argparse, os, sys
from datetime import datetime, timedelta
from ConfigParser import ConfigParser
import numpy as np
import sqlite3
from base.misc import xdays
from base.farside.pyComp import plot_seismic_image



if __name__ == "__main__":
    # Annita added the line above

    # --- argparse ---
    parser = argparse.ArgumentParser(description='plot farside composite images',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-D', '--dbfile', type=str, default=None, help="""
            Path to the sqlite database. Expect the filename extension to be "db".
            Overwrite the environment variable GOSIP_DBFILE, if exists.
            """)
    parser.add_argument('cfgfile', type=str, help='configuration file')
    parser.add_argument('first', type=str, help='first date in format of %%Y-%%m-%%d %%H:%%M:%%S')
    parser.add_argument('last', type=str, help='last date (included) in format of %%Y-%%m-%%d %%H:%%M:%%S')
    args = parser.parse_args()

    dbfile = args.dbfile
    if dbfile: # overwrite GOSIP_DBFILE
        os.environ['GOSIP_DBFILE'] = dbfile
    elif 'GOSIP_DBFILE' in os.environ:
        dbfile = os.environ['GOSIP_DBFILE']
    else:
        raise RuntimeError('GOSIP_DBFILE is not set')
    cfgfile = args.cfgfile
    first = datetime.strptime(args.first, '%Y-%m-%d %H:%M:%S')
    last  = datetime.strptime(args.last,  '%Y-%m-%d %H:%M:%S')
    print('dbfile: {}'.format(dbfile))
    print('cfgfile: {}'.format(cfgfile))
    print('first: {}'.format(first))
    print('last:  {}'.format(last))

    if not os.path.exists(dbfile):
        raise RuntimeError('GOSIP_DBFILE does not exist: {}'.format(dbfile))
    con = sqlite3.connect(dbfile)
    rows = con.execute("""
        SELECT date,has_79hr FROM farside WHERE date >= ? AND date <= ?
    """, [first, last]).fetchall()
    con.close()
    assert len(rows) == (last - first).total_seconds()/43200. + 1
    dates, has_79hr = np.asarray(rows).T
    has_79hr = has_79hr.astype(bool)

    if not os.path.exists(cfgfile):
        raise RuntimeError('cfgfile does not exist: {}'.format(cfgfile))
    cfg = ConfigParser()
    cfg.read(cfgfile)
    farside_dir = cfg.get('dataParam', 'farside_dir')
    gaussian_smooth_3day_fits = cfg.getboolean('dataParam', 'gaussian_smooth_3day_fits')
    print('farside_dir:', farside_dir)
    print('gaussian_smooth_3day_fits:', gaussian_smooth_3day_fits)
    sys.stdout.flush()

    T = [datetime.now()]
    stop = last + timedelta(days=0.5)
    for it,dat in enumerate(xdays(first, stop, timedelta(days=0.5))):
        assert dates[it] == str(dat)
        if not has_79hr[it]:
            print('Skip {}: has_79hr is False.'.format(dat))
            continue

        yyp = dat.year
        mmp = dat.month
        ddp = dat.day
        halfday = (dat.hour == 12)
        plot_seismic_image(yy=yyp, mm=mmp, dd=ddp, halfday=halfday,
                XLABEL=True, withsecant=False, withcb=True, fz=20, DT_indays=3,
                Gaussian_smooth=gaussian_smooth_3day_fits, use_abs=False,
                noylabel=False, two_row=False, farside_dir=farside_dir)
        T.append(datetime.now())
        print('Elapsed time: {} for plotting farside image of {}'.format(T[-1]-T[-2], dat))
        sys.stdout.flush()
