#!/usr/bin/env python

"""
This documentation is for main.py. Inline documentations are missing.  
"""

from __future__ import print_function
from pyComp import * # dangerous!
import argparse, os, sys
from datetime import datetime, timedelta
from astropy.time import Time
from astropy.table import Table, Column
import numpy as np
import ConfigParser
import sqlite3

from base.misc import xdays



T = [datetime.now()]

print('Parse arguments')
print('---------------')
sys.stdout.flush()


if __name__ == "__main__":
    
    # Annita added the if _name for sphinx to run
    
    parser = argparse.ArgumentParser(description='compute far-side images',
            formatter_class=argparse.RawDescriptionHelpFormatter,
            epilog="""
    environment variables in use:
    PATH should include $GOSIP_DIR/base/farside/c_code_charlie
    PYTHONPATH should include $GOSIP_DIR/base/farside:$GOSIP_DIR
    LD_LIBRARY_PATH should include /opt/local/cfitsio/cfitsio-3.350/lib
    GOSIP_TMPDIR used by slurm
    GOSIP_DBFILE to be updated
    """)
    parser.add_argument('--only_reference', action='store_true', help='compute only the reference')
    parser.add_argument('-D', '--dbfile', type=str, default=None, help="""
            Path to the sqlite database. Expect the filename extension to be "db".
            If not given, use the environment variable GOSIP_DBFILE.""")
    parser.add_argument('cfgfile', type=str, help='configuration file')
    parser.add_argument('first', type=str, help='first date in format of %%Y-%%m-%%d %%H:%%M:%%S')
    parser.add_argument('last', type=str, help='last date (included) in format of %%Y-%%m-%%d %%H:%%M:%%S')
    args = parser.parse_args()
    compute_only_reference = args.only_reference
    dbfile = args.dbfile
    initFileName = args.cfgfile
    first = args.first
    last = args.last

    if not dbfile:
        if 'GOSIP_DBFILE' in os.environ:
            dbfile = os.environ['GOSIP_DBFILE']
        else:
            raise RuntimeError('GOSIP_DBFILE is not set')
    print('compute_only_reference: {}'.format(compute_only_reference))
    print('dbfile: {}'.format(dbfile))
    print('initFileName: {}'.format(initFileName))
    print('first: {}'.format(first))
    print('last:  {}'.format(last))
    sys.stdout.flush()

    # =====================================================
    # =============== read config file  ===================
    # =====================================================
    print('')
    print('Parse config file')
    print('-----------------')
    sys.stdout.flush()

    config                    = ConfigParser.ConfigParser()
    config.read(initFileName)
    dstep                     = config.getint('dataParam', 'dstep') # units hr
    farside_dir               = config.get('dataParam', 'farside_dir')
    farside_reference_dir     = config.get('dataParam', 'farside_reference_dir')
    greens_dir                = config.get('dataParam', 'greens_dir')
    output_nth                = config.getint('dataParam', 'output_nth')
    output_nph                = config.getint('dataParam', 'output_nph')
    nproc_reduction           = config.getint('dataParam', 'nproc_reduction')
    nproc_h_images            = config.getint('dataParam', 'nproc_h_images')

    clean_run                 = config.getboolean('dataParam', 'clean_run')
    save_postel_fits          = config.getboolean('dataParam', 'save_postel_fits')
    save_corr_fits            = config.getboolean('dataParam', 'save_corr_fits')
    gaussian_smooth_1day_fits = config.getboolean('dataParam', 'gaussian_smooth_1day_fits')
    gaussian_smooth_3day_fits = config.getboolean('dataParam', 'gaussian_smooth_3day_fits')
    nsigma_gs                 = config.getint('dataParam', 'nsigma_gs')

    print('dstep:', dstep)
    print('farside_dir:', farside_dir)
    print('farside_reference_dir:', farside_reference_dir)
    print('greens_dir:', greens_dir)
    print('output_nth:', output_nth)
    print('output_nph:', output_nph)
    print('nproc_reduction:', nproc_reduction)
    print('nproc_h_images:', nproc_h_images)
    print('clean_run:', clean_run)
    print('save_postel_fits:', save_postel_fits)
    print('save_corr_fits:', save_corr_fits)
    print('gaussian_smooth_1day_fits:', gaussian_smooth_1day_fits)
    print('gaussian_smooth_3day_fits:', gaussian_smooth_3day_fits)
    print('nsigma_gs:', nsigma_gs)
    sys.stdout.flush()

    # =====================================================
    # =============== obtain time range ===================
    # =====================================================
    dstart  = datetime.strptime(first, '%Y-%m-%d %H:%M:%S')
    dstop   = datetime.strptime(last,  '%Y-%m-%d %H:%M:%S') + timedelta(hours=dstep)
    dstop  += timedelta(hours=24) # stop one day after, as needed for the 79-hr measurement.
    dstart -= timedelta(hours=24) # start one day earlier as needed for the 79-hr measurement

    if not os.path.exists(dbfile):
        raise RuntimeError('GOSIP_DBFILE does not exist: {}'.format(dbfile))
    sqlite3.register_adapter(np.int64, int)
    sqlite3.register_adapter(np.int32, int)
    con = sqlite3.connect(dbfile)
    infmt = dict(con.execute('SELECT * from farside_meta').fetchall())
    rows = con.execute("""
        SELECT date,has_postel,has_corr,has_31hr,has_79hr,N_miss,N_corr,L0_ref,B0_ref FROM farside WHERE date >= ? AND date < ?
    """, [dstart, dstop]).fetchall()
    con.close()
    tab = Table(rows=rows,
            names=['date','has_postel','has_corr','has_31hr','has_79hr','N_miss','N_corr','L0_ref','B0_ref'],
            #dtype=['U19', bool, bool, bool, bool, int, int, float, float], # not working for NULL
            )
    assert len(tab) == (dstop - dstart).total_seconds()/43200.
    assert infmt['fmt_postel'].startswith(farside_dir)
    assert infmt['fmt_corr'].startswith(farside_dir)
    assert infmt['fmt_31hr'].startswith(farside_dir)
    assert infmt['fmt_79hr'].startswith(farside_dir)

    print('')
    print('List of time stamps to be processed:')
    dats, JD_time_reduction = [], []
    for it,dat in enumerate(xdays(dstart, dstop, timedelta(hours=dstep))):
        assert tab['date'][it] == str(dat)
        dats.append(dat)
        jd = Time(dat, format='datetime', scale='tai').mjd
        if clean_run or not os.path.exists(dat.strftime(infmt['fmt_postel'])):
            JD_time_reduction.append(jd)
            print('{:4} {} add to reduction list'.format(it+1, dat))
        else:
            print('{:4} {} skip reduction'.format(it+1, dat))
    sys.stdout.flush()

    if len(dats) < 5:
        raise RuntimeError('At least 5 timestamps are needed to compute one 79-hr farside image')

    # =====================================================
    # ===============  data reduction   ===================
    # =====================================================
    if len(JD_time_reduction) > 0:
        print('-----------------------------')
        print('Data reduction (Postel cubes)')

        # change nproc_reduction if bigger than actual process needed
        nproc_reduction = np.min([nproc_reduction, np.size(JD_time_reduction)])
        print('Using {} cpus for data reduction'.format(nproc_reduction))

        # walltime=40min=0.7hr per one 31-hr cube
        walltime = np.ceil(np.size(JD_time_reduction)/float(nproc_reduction)) * 0.7 # [hr]
        walltime = np.ceil(walltime)
        if walltime > 48:
            raise RuntimeError('helio cluster does not accept job walltime longer than 2-d')
        print('Estimated walltime for data reduction is rounded up to {} hr'.format(walltime))

        T.append(datetime.now())
        print('Elapsed time: {} for reading the parameters'.format(T[-1]-T[-2]))
        sys.stdout.flush()

        imageSizeGB = 4
        TIME_CLASS = {'JD': np.array(JD_time_reduction)}
        Postel_fitsname = reduceOnCluster(Postel_job,\
                (TIME_CLASS, farside_dir, clean_run, np.arange(np.size(JD_time_reduction))),\
                len(JD_time_reduction),  nproc_reduction,\
                imageSizeGB*1024*1024, deleteFiles=True, walltime=walltime)
        T.append(datetime.now())
        print('Elapsed time: {} for data reduction'.format(T[-1]-T[-2]))
    else:
        print('------------------------------')
        print('No data reduction is performed')
    sys.stdout.flush()

    # =====================================================
    # ===============  reference images  ==================
    # =====================================================
    if compute_only_reference:
        print('-------------------')
        print('Computing reference')
        FAR_REF = farside_reference(farside_reference_dir =  farside_reference_dir, postel_dir = farside_dir, Greensfuc_dir = greens_dir)
        reference_phase, th = FAR_REF()
        T.append(datetime.now())
        print('Elapsed time: {} for computing reference'.format(T[-1]-T[-2]))

    # =====================================================
    # ===============  far-side images  ===================
    # =====================================================
    if not compute_only_reference:
        print('-------------------------')
        print('Computing far-side images')
        CORR_fitsname = [] # used for cleaning
        F_image_1day = seismic_image_farside(do_smooth =  gaussian_smooth_1day_fits, nsigma = nsigma_gs, farsidedir = farside_dir, farside_reference_dir = farside_reference_dir, Greensfuc_dir = greens_dir, output_nth  = output_nth, output_nph  = output_nph, nbProc = nproc_h_images)
        F_image_3day = seismic_image_farside(do_smooth =  gaussian_smooth_3day_fits, nsigma = nsigma_gs, farsidedir = farside_dir, farside_reference_dir = farside_reference_dir, Greensfuc_dir = greens_dir, output_nth  = output_nth, output_nph  = output_nph, nbProc = nproc_h_images)
        for it,dat in enumerate(dats):
            divider = 'Processing {} ({}/{})'.format(dat, it+1, len(dats))
            print('-'*len(divider))
            print(divider)
            yy = dat.year
            mm = dat.month
            dd = dat.day
            halfday = (dat.hour == 12)

            if clean_run or not os.path.exists(dat.strftime(infmt['fmt_corr'])):
                print('computing corr for {}'.format(dat) )
                sys.stdout.flush()
                corr_filename  = F_image_1day.compute_CORR_(yy = yy, mm = mm, dd = dd, halfday = halfday, Fcompute = clean_run)
                T.append(datetime.now())
                print('Elapsed time: {} for computing CORR image of {}'.format(T[-1]-T[-2], dat))
                assert corr_filename == dat.strftime(infmt['fmt_corr'])
                CORR_fitsname.append(corr_filename)

            else:
                print('skipping corr for {}'.format(dat) )
                corr_filename = dat.strftime(infmt['fmt_corr'])
                assert os.path.exists(corr_filename)

            if os.path.isfile(corr_filename):

                f1d, mesh = F_image_1day(yy = yy, mm = mm, dd = dd, DT_indays = 1, halfday = halfday, Fcompute = clean_run)

                T.append(datetime.now())
                print('Elapsed time: {} for computing 31-hr farside image of {}'.format(T[-1]-T[-2], dat))

            # far-side images for 79 hr
            if it >= 4: # start to compute after 5 timestamps are done
                # datp: backward one day (two timestamps) to compute the 79-hr
                #       at the middle timestamp for the past 5 timestamps
                datp = dat - timedelta(days=1)
                yyp = datp.year
                mmp = datp.month
                ddp = datp.day
                assert datp.hour == dat.hour
                f3d, mesh = F_image_3day(yy = yyp, mm = mmp, dd = ddp, DT_indays = 3, halfday = halfday, Fcompute = clean_run)

                T.append(datetime.now())
                print('Elapsed time: {} for computing 79-hr farside image of {}'.format(T[-1]-T[-2], datp))
            sys.stdout.flush()

        # =====================================================
        # ===============  cleaning files   ===================
        # =====================================================
        if not save_corr_fits:
            for corr_file in CORR_fitsname:
                print('Removing {}'.format(corr_file))
                remove(corr_file)
            T.append(datetime.now())
            print('Elapsed time: {} for removing {} CORR image(s)'.format(T[-1]-T[-2], len(CORR_fitsname)))

    for it,dat in enumerate(dats):
        filename = dat.strftime(infmt['fmt_postel'])
        if os.path.isfile(filename):
            h = fits.getheader(filename)
            tab['L0_ref'][it] = h['ref_L0']
            tab['B0_ref'][it] = h['ref_B0']
            tab['N_miss'][it] = h['NMISS'] if 'NMISS' in h else None
    if not save_postel_fits:
        for postel_file in Postel_fitsname:
            print('Removing {}'.format(postel_file))
            remove(postel_file)
        T.append(datetime.now())
        print('Elapsed time: {} for removing {} Postel image(s)'.format(T[-1]-T[-2], len(Postel_fitsname)))

    print('End of computation')
    sys.stdout.flush()

    # =====================================================
    # =================  update DBFILE  ===================
    # =====================================================
    for it,dat in enumerate(dats):
        filename = dat.strftime(infmt['fmt_postel'])
        tab['has_postel'][it] = os.path.isfile(filename)
        filename = dat.strftime(infmt['fmt_corr'])
        tab['has_corr'][it] = os.path.isfile(filename)
        filename = dat.strftime(infmt['fmt_31hr'])
        tab['has_31hr'][it] = os.path.isfile(filename)
        filename = dat.strftime(infmt['fmt_79hr'])
        tab['has_79hr'][it] = os.path.isfile(filename)
        if tab['has_79hr'][it]:
            h = fits.getheader(filename)
            assert h['N_CORR'].is_integer()
            tab['N_corr'][it] = int(h['N_CORR'])
    tab.pprint(max_lines=-1, max_width=-1)
    con = sqlite3.connect(dbfile)
    with con:
        con.executemany("""
            UPDATE farside
            SET has_postel = ?,
                has_corr = ?,
                has_31hr = ?,
                has_79hr = ?,
                N_miss = ?,
                N_corr = ?,
                L0_ref = ?,
                B0_ref = ?
            WHERE date = ?
        """, tab['has_postel','has_corr','has_31hr','has_79hr','N_miss','N_corr','L0_ref','B0_ref','date'])
    con.close()
    T.append(datetime.now())
    print('Elapsed time: {} for updating {} row(s) in the table "farside" in DBFILE'.format(T[-1]-T[-2], len(tab)))

    T.append(datetime.now())
    print('Elapsed time: {} for the whole main.py'.format(T[-1]-T[0]))
    sys.stdout.flush()
