# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os
import sys
sys.path.insert(0, os.path.abspath('../../Farside_sphinx/'))
sys.path.insert(0, os.path.abspath('../../Farside_sphinx/subfolder'))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Gosip test'
copyright = '2025, None'
author = 'None'
release = '0.001'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

# extensions = [
#     'sphinx.ext.autodoc',
#     'sphinx.ext.viewcode',
#     'sphinx.ext.napoleon'
# ]

extensions = [
     'numpydoc',
     'sphinx.ext.viewcode'
 ]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# this is to state that all import modules should be ignored
autodoc_mock_imports = ['astropy', 'numpy', 'base', 'pyComp', 'ConfigParser', 'pymysql']


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
