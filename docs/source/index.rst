.. Gosip test documentation master file, created by
   sphinx-quickstart on Tue Jan  2 11:39:27 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the GoSiP Farside Documentation page
===============================================


Here you will find a comprehensive guide to the codes used for the farside project.  


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Farside_code:

   readmesphinx
   main
   updatedb
   plotfarsideimages


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Database:

   updatemysql
   updatemysql1A

   .. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Farside_website:
   
   readmefarsidewebsite

