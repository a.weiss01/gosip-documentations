
README 
#######
DATES: 30 NOV.  2023

Below are the steps to create a sphinx documentation
====================================================

STEP 1: Create a virtual environment in WSL2 (UBUNTU)::

    -> pip3 install --user virtualenv
    -> ~/.local/bin/virtualenv .env
    -> source .env/bin/activate

STEP 2: Install the libraries::

    -> pip3 install -r requirements.txt 
    -> pip3 install sphinx
    -> pip3 install sphinx_rtd_theme

STEP 3: Create sphinx make.bat, makefile, config.py files:

** Use sphinx-quickstart to generate Sphinx source directory with conf.py and index.rst::

    -> mkdir docs
    -> cd docs
    -> sphinx-quickstart


Runing sphinx-quickstart will present to you a series of questions required to create the basic directory and configuration layout for your project inside the docs folder.::

    To proceed, answer each question as follows:
    > Separate source and build directories (y/n) [n]: Write “y” (without quotes) and press Enter.
    > Project name: Write “Lumache” (without quotes) and press Enter.
    > Author name(s): Write “Graziella” (without quotes) and press Enter.
    > Project release []: Write “0.1” (without quotes) and press Enter.
    > Project language [en]: Leave it empty (the default, English) and press Enter.

Check the creation of files::

    > docs$ ls

Check the creation of build,  make.bat, Makefile, and  source::

    > cd source
    > docs/source$ ls
    conf.py  index.rst  instalation.rst

STEP 4: Configure the conf.py file::

    ->  go inside the conf.py file add these three lines: 
    Explanation: page 14  ref:  https://readthedocs.org/projects/sphinx-rtd-tutorial/downloads/pdf/latest/

    ''' import os
        import sys
        sys.path.insert(0, os.path.abspath('../../Farside_sphinx/')) '''


STEP 5: Use sphinx-apidoc to generate reStructuredText files from source code
Note: your code.py files are located in this instance in Farside_sphinx folder::

    > cd docs
    > docs$ sphinx-apidoc -f -o source/  ../Farside_sphinx/


STEP 6: Edit index.rst and the generated reStructuredText files::

    > See ref: https://brendanhasz.github.io/2019/01/05/sphinx.html
    > under "Writing the documentation"


STEP 7: Build  and visualise documents::
    
    > cd docs
    > docs$ make html

To clean the html files::
 
    > docs$ make clean  


STEP 8: Update the Repository in gitlab

    > Place your .py files in Farside_sphinx
    > Update the repository in GiTlab 
    > This will update the website

REFERENCES and TIPS: 

Can the documentation be automatic?
  * To add another documented code.py, simply add the file in the Farside_sphinx folder and run step 5, 6, and 7. 

Update a code.py file
  * After you have modified your code.py file just run step 7. 

Instructions on how to write Sphinx's docstring can be found here. 
  * For docstring similar to numpy fft, the ref: https://numpydoc.readthedocs.io/en/latest/format.html
  * Ref:  https://readthedocs.org/projects/sphinx-rtd-tutorial/downloads/pdf/latest/ 

Tip: File name formats.
  * Files name should be in one word,  no dash, no underscore. 

Tip: Files not compiling.
  * An error may occur if some packages cannot the imported. Sphinx does required all packages to be compiled. However, this  may not be possible on all computer. 
  Thus, it is necessary to mock import these packages.
   See ref:  https://brendanhasz.github.io/2019/01/05/sphinx.html#mock-importing-packages


